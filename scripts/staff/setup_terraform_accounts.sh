#!/bin/bash -eu

SCRIPT_DIR=$(cd $(dirname $0); pwd)

users="$@"
. ${SCRIPT_DIR}/common.sh

function setup_delivery_terraform_accounts() {
    local user_name=$1
    local base_path=$(git rev-parse --show-toplevel)
    local delivery_backend_bucket_name=${user_name}-delivery-backend

    create_s3_bucket ${delivery_backend_bucket_name} ${AWS_DEFAULT_REGION}
    cd ${base_path}/${user_name}/setup_terraform_accounts/delivery
    terraform init --backend-config=./backend.config
    terraform import -lock=false 'module.delivery.aws_s3_bucket.backend_itself' ${delivery_backend_bucket_name}
    terraform apply -lock=false --auto-approve

    cd ${SCRIPT_DIR}
    create_user_access_key ${base_path} ${user_name}
}

function setup_staging_terraform_accounts() {
    local user_name=$1
    local base_path=$(git rev-parse --show-toplevel)
    local staging_backend_bucket_name=${user_name}-staging-backend

    create_s3_bucket ${staging_backend_bucket_name} ${AWS_DEFAULT_REGION}
    cd ${base_path}/${user_name}/setup_terraform_accounts/runtimes/staging
    terraform init --backend-config=./backend.config
    terraform import -lock=false 'module.backend.aws_s3_bucket.tfstate' ${staging_backend_bucket_name}
    terraform apply -lock=false --auto-approve
}

check_tools

for user in ${users}; do
  echo ${user}
  set_delivery_user_credential ${user}
  setup_delivery_terraform_accounts ${user}
  echo "delivery_done"
  echo ${user}
  set_staging_user_credential ${user}
  setup_staging_terraform_accounts ${user}
  echo "staging_done"
done

